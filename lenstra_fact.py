from point import Point
from random import randint
from datetime import datetime

import math
import sympy
import utils

def initPoint(N):
    p = Point(0, 0, 1)
    while not p.x and not p.y:
        p.x = randint(0, N-1)
        p.y = randint(0, N-1)
    f.write("#####################################################\n")
    f.write("initPoint: x = " + str(p.x) + "; y = " + str(p.y) + '\n')
    return p

### E : Y^2 = X^3 + AX + B

def initCurve(p, N):
    coefA = 0
    coefB = 0
    while ( 4 * coefA ** 3 + 27 * coefB ** 2 ) % N == 0:
        coefA = randint(0, N-1)
        coefB = ( p.y**2 - p.x**3 - coefA * p.x ) % N
    f.write("initCurve: coefA = " + str(coefA) + "; coefB = " + str(coefB) + '\n')
    return coefA, coefB

def findFactor(p1, a, b, N, param):
    for j in range(2, param):
        p2 = p1.mul(j, a, b, N)
        f.write("point: x = " + str(p2.x) + "; y = " + str(p2.y) + '\n')
        if p2.is_zero():
            d = math.gcd(p2.x, N)
            if d == N:
                return -1
            elif d < N:
                return d
        else:
            p1 = p2
    return -1

def factorization(N):
    f.write("#####################################################\n")
    f.write("-----------------------------------------------------\n")
    f.write("\tNew Num = " + str(N) + "\n")
    f.write("-----------------------------------------------------\n")
    fList = []
    res = N
    while (not sympy.isprime(res)) and (res != 1):
        p = initPoint(res)
        curveCoefA, curveCoefB = initCurve(p, res)
        d = findFactor(p, curveCoefA, curveCoefB, res, round(math.sqrt(N)))
        if d != -1 and d != 1:
            res //= d
            fList.append(d)
    fList.append(res)
    return fList

def onlyPrime(fList):
    res = []
    allPrime = False
    while not allPrime:
        allPrime = True
        for fact in fList:
            if sympy.isprime(fact):
                res.append(fact)
            else:
                allPrime = False
                res += factorization(fact)
        fList = res
    return res

#####

f = open("log", "w")

cnt = 0
rng = 3
for i in range(rng):
    val = 10e22
    toFactNum = utils.getTestNumber3(val, round(math.sqrt(val)))
    #toFactNum = utils.getTestNumber2(val, 100)
    print("Num:", toFactNum)
    start_time = datetime.now()
    print("start_time:", start_time)
    fList = factorization(toFactNum)
    #print("find non-trivial factor")
    #onlyPrime(fList)
    fList.sort()
    print("  end_time:", datetime.now())
    print("fList:", fList)
    for fact in fList:
        print(fact, "prime:", sympy.isprime(fact))
    if toFactNum == utils.mul(fList):
        cnt += 1
    print("---------------------------------")

print(rng == cnt)

f.write("#####################################################\n")
f.close()






















