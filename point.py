from sympy import mod_inverse

"""
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def invert(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise ZeroDivisionError
    else:
        return x % m
"""

class Point:

    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z
 
    def copy(self):
        return Point(self.x, self.y, self.z)
 
    def is_zero(self):
        return self.z == 0
 
    def neg(self):
        return Point(self.x, -self.y, self.z)
 
    def dbl(self, a, b, N):
        if self.is_zero():
            return self.copy()
        try:
            L = ((3 * self.x * self.x + a) * mod_inverse(2 * self.y, N)) % N # invert
        except ValueError: #ZeroDivisionError:
            return Point(2 * self.y, 0, 0)
        x = ( L * L - 2 * self.x ) % N
        y = ( L * (self.x - x) - self.y ) % N
        return Point(x, y, 1)
 
    def add(self, q, a, b, N):
        if self.x == q.x and self.y == q.y:
            return self.dbl(a, b, N)
        if self.is_zero():
            return q.copy()
        if q.is_zero():
            return self.copy()
        try:
            L = ((q.y - self.y) * mod_inverse(q.x - self.x, N)) % N # invert
        except ValueError: #ZeroDivisionError:
            return Point(q.x - self.x, 0, 0)
        x = ( L * L - self.x - q.x ) % N
        y = ( L * (self.x - x) - self.y ) % N
        return Point(x, y, 1)
 
    def mul(self, n, a, b, N):
        p = self.copy()
        r = Point()
        i = 1
        while i <= n:
            if i&n:
                r = r.add(p, a, b, N)
            p = p.dbl(a, b, N)
            i <<= 1
        return r

    def show(self):
        print(self.x, self.y, self.z)
