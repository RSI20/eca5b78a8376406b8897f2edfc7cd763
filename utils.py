from random import randint

import math

def getTestNumber1(val, param):
    res = val * 100
    m = 0
    while res > val * 10:
        res = 1
        while res < val:
            m = randint(5, param)
            res *= m
            if math.gcd(res, 6) != 1:
                while not res % 2:
                    res //= 2
                while not res % 3:
                    res //= 3
        res * (m + 1)
    return res

def getTestNumber2(val, param):
    res = 1
    while res < val:
        m = 2
        while math.gcd(m, 6) != 1 or math.gcd(m + 2, 6) != 1:
            m = randint(5, param)
        res *= m * (m + 2)
        if res > val * 10:
            res = 1
    return res

def getTestNumber3(val, param):
    return randint(val, val * 10)

def mul(lst):
    res = 0
    if lst:
        res = 1
        for i in lst:
            res *= i
    return res

def isPrime(n):
    if (n <= 1):
        return False
    if (n == 2):
        return True
    if (n % 2 == 0):
        return False

    i = 3
    while i <= math.sqrt(n):
        if n % i == 0:
            return False
        i = i + 2

    return True
